package com.example.practica02;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button btnIMC;
    private Button btnClean;
    private Button btnClose;
    private EditText txtAltura;
    private EditText txtPeso;
    private TextView lblIMC;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnIMC = (Button) findViewById(R.id.btnCalcular);
        txtAltura = (EditText) findViewById(R.id.txtAltura);
        txtPeso = (EditText) findViewById(R.id.txtPeso);
        lblIMC= (TextView) findViewById(R.id.lblIMC);
        btnClean = (Button) findViewById(R.id.btnLimpiar);
        btnClose = (Button) findViewById(R.id.btnCerrar);


        btnIMC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtAltura.getText().toString().matches("") || txtPeso.getText().toString().matches("") ){
                    Toast.makeText(MainActivity.this,"Faltó capturar información", Toast.LENGTH_SHORT).show();
                }
                else{
                    String alt = txtAltura.getText().toString();
                    String pes = txtPeso.getText().toString();
                    float altu = Float.parseFloat(alt);
                    float peso = Float.parseFloat(pes);
                    float altur = altu * altu;
                    float calculo = peso / altur;
                    String str = "Tu IMC es: " + calculo + " Kg/m^2";
                    lblIMC.setText(str.toString());
                }
            }
        });

        btnClean.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtAltura.setText("");
                txtPeso.setText("");
                lblIMC.setText("");
            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}